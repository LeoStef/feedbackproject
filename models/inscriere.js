const configuration = require("../config/configuration.json")
const Sequelize = require("sequelize")

const DB_NAME = configuration.database.database_name
const DB_USER = configuration.database.username
const DB_PASS = configuration.database.password

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
	dialect: "mysql"
})

sequelize
	.authenticate()
	.then(() => {
		console.log("Database connection success!")
	})
	.catch(err => {
		console.log(`Database connection error: ${err}`)
	})

class Inscriere extends Sequelize.Model {}

Inscriere.init(
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		idActivitate: {
			type: Sequelize.INTEGER,
			allowNull: false
		},
		idUtilizator: {
			type: Sequelize.INTEGER,
			allowNull: false
		}
	},
	{
		sequelize,
		modelName: "inscrieri"
	}
)

// Inscriere.sync({ force: true })

module.exports = {
	sequelize,
	Inscriere
}

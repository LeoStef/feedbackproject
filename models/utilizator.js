const configuration = require("./../config/configuration.json")
const Sequelize = require("sequelize")

const DB_NAME = configuration.database.database_name
const DB_USER = configuration.database.username
const DB_PASS = configuration.database.password

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
	dialect: "mysql"
})

sequelize
	.authenticate()
	.then(() => {
		console.log("Database connection success!")
	})
	.catch(err => {
		console.log(`Database connection error: ${err}`)
	})

class Utilizator extends Sequelize.Model {}

Utilizator.init(
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		nume: {
			type: Sequelize.STRING,
			allowNull: false
		},
		prenume: {
			type: Sequelize.STRING,
			allowNull: false
		},
		parola: {
			type: Sequelize.STRING,
			allowNull: false
		},
		email: {
			type: Sequelize.STRING,
			allowNull: false,
			validate: {
				isEmail: true
			}
		},
		esteProfesor: {
			type: Sequelize.BOOLEAN,
			allowNull: false
		}
	},
	{
		sequelize,
		modelName: "utilizatori"
	}
)

// Utilizator.sync({ force: true })

module.exports = {
	sequelize,
	Utilizator
}

const { Utilizator } = require("./../models/utilizator")

const utilizator = {
	getByEmail: async email => {
		try {
			return await Utilizator.findOne({ where: { email: email } })
		} catch (err) {
			throw new Error(err.message)
		}
	},
	create: async utilizator => {
		try {
			return await Utilizator.create(utilizator)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = utilizator

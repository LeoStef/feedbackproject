const { Inscriere } = require("../models/inscriere")
const { Activitate } = require("../models/activitate")

const inscriere = {
	create: async inscriere => {
		try {
			return await Activitate.findOne({
				where: {
					id: parseInt(inscriere.cod)
				}
			}).then(async activitate => {
				if (activitate) {
					return await Inscriere.create({
						idUtilizator: inscriere.idUtilizator,
						idActivitate: activitate.id
					})
				}
			})
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = inscriere

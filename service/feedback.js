const { Feedback } = require("../models/feedback")

const feedback = {
	get: async idActivitate => {
		return await Feedback.findAll({
			where: {
				idActivitate: idActivitate
			}
		}).then(function(feedbacks) {
			return feedbacks
		})
	},
	create: async feedback => {
		try {
			return await Feedback.create({ data: new Date(), ...feedback })
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = feedback

const { Activitate } = require("../models/activitate")
const { Inscriere } = require("../models/inscriere")
const { Utilizator } = require("../models/utilizator")

const activitate = {
	getById: async id => {
		return await Activitate.findAll().then(async function(activitati) {
			let utilizator = await Utilizator.findOne({ where: { id: id } })
			if (utilizator.esteProfesor) {
				let activitatiTrecute = activitati.filter(
					activitate =>
						new Date(
							activitate.data.getTime() +
								parseInt(activitate.timp.split(":")[0] * 60 + activitate.timp.split(":")[1]) * 60000
						) < new Date()
				)
				let activitatiActuale = activitati.filter(
					activitate =>
						new Date(
							activitate.data.getTime() +
								parseInt(activitate.timp.split(":")[0] * 60 + activitate.timp.split(":")[1]) * 60000
						) > new Date()
				)
				return {
					trecute: activitatiTrecute,
					actuale: activitatiActuale
				}
			} else {
				return await Inscriere.findAll({
					where: { idUtilizator: parseInt(id) }
				}).then(function(inscrieri) {
					let activitatiTrecute = activitati.filter(
						activitate =>
							inscrieri.find(
								inscriere =>
									activitate.id === inscriere.idActivitate &&
									inscriere.idUtilizator === parseInt(id)
							) &&
							new Date(
								activitate.data.getTime() +
									parseInt(activitate.timp.split(":")[0] * 60 + activitate.timp.split(":")[1]) *
										60000
							) < new Date()
					)
					let activitatiActuale = activitati.filter(
						activitate =>
							inscrieri.find(
								inscriere =>
									activitate.id === inscriere.idActivitate &&
									inscriere.idUtilizator === parseInt(id)
							) &&
							new Date(
								activitate.data.getTime() +
									parseInt(activitate.timp.split(":")[0] * 60 + activitate.timp.split(":")[1]) *
										60000
							) > new Date()
					)
					let alteActivitati = activitati.filter(
						activitate =>
							!activitatiActuale.find(actuala => actuala.id === activitate.id) &&
							!activitatiTrecute.find(trecuta => trecuta.id === activitate.id) &&
							new Date(
								activitate.data.getTime() +
									parseInt(activitate.timp.split(":")[0] * 60 + activitate.timp.split(":")[1]) *
										60000
							) > new Date()
					)
					return {
						trecute: activitatiTrecute,
						actuale: activitatiActuale,
						altele: alteActivitati
					}
				})
			}
		})
	},
	create: async activitate => {
		try {
			return await Activitate.create(activitate)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = activitate

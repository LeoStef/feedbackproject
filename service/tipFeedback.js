const { TipFeedback } = require("../models/tipFeedback")

const tipFeedback = {
	get: async () => {
		return await TipFeedback.findAll().then(function(tipFeedbacks) {
			return tipFeedbacks
		})
	}
}

module.exports = tipFeedback

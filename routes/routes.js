const express = require("express")
const router = express.Router()
const { getUtilizator } = require("../controller/utilizator")
const { getActivitate, createActivitate } = require("../controller/activitate")
const { createInscriere } = require("../controller/inscriere")
const { getFeedback, createFeedback } = require("../controller/feedback")
const { getTipFeedback } = require("../controller/tipFeedback")

router.get("/utilizator/:email", getUtilizator)
router.get("/activitate/:id", getActivitate)
router.get("/tipFeedback", getTipFeedback)
router.get("/feedback/:idActivitate", getFeedback)
router.post("/activitate", createActivitate)
router.post("/inscriere", createInscriere)
router.post("/feedback", createFeedback)

module.exports = router

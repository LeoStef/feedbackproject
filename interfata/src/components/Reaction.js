import React from "react"
import axios from "axios"

class Reaction extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}

		this.handleReaction = this.handleReaction.bind(this)
	}

	handleReaction = event => {
		axios.post("https://19812b1dec7c456aba654301513f235c.vfs.cloud9.us-east-2.amazonaws.com/api/feedback", {
			idTip: this.props.id,
			idActivitate: this.props.idActivitate
		})
	}
	render() {
		return (
			<div className='mt-5'>
				<button
					className='btn btn-danger mr-2'
					style={{ width: 100 + "%", height: 200 + "px" }}
					onClick={this.handleReaction}
				>
					{this.props.nume}
				</button>
			</div>
		)
	}
}
export default Reaction

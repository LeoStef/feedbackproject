import React from "react"
import axios from "axios"

import ListaReactionProfesor from "./ListaReactionProfesor"
let timer

class ActivitateProfesor extends React.Component {
	state = {}

	componentDidMount = () => {
		axios.get("https://19812b1dec7c456aba654301513f235c.vfs.cloud9.us-east-2.amazonaws.com/api/feedback/" + this.props.id).then(feedbacks => {
			this.setState({
				feedbacks: feedbacks.data
			})
		})
		timer = setInterval(() => {
			axios.get("https://19812b1dec7c456aba654301513f235c.vfs.cloud9.us-east-2.amazonaws.com/api/feedback/" + this.props.id).then(feedbacks => {
				this.setState({
					feedbacks: feedbacks.data
				})
			})
		}, 1000)
	}

	componentWillUnmount = () => {
		clearInterval(timer)
	}

	render() {
		return (
			<>
				<div className='ml-3'>
					<h2>{this.props.descriere}</h2>
					<div>Data: {new Date(this.props.data).toLocaleDateString()}</div>
					<button className='btn btn-sm btn-warning mt-2' onClick={this.props.inapoi}>
						Inapoi
					</button>
				</div>
				<br />
				<br />
				<ListaReactionProfesor
					idActivitate={this.props.id}
					tipFeedbacks={this.props.tipFeedbacks}
					feedbacks={this.state.feedbacks || []}
				></ListaReactionProfesor>
			</>
		)
	}
}

export default ActivitateProfesor

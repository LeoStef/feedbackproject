import React from "react"
import axios from "axios"

import Activitate from "./Activitate"
import DashboardMode from "./DashboardMode"
import AdaugareActivitate from "./AdaugareActivitate"
import ActivitateProfesor from "./ActivitateProfesor"

export class Home extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			tipFeedbacks: [],
			trecute: [],
			actuale: [],
			altele: [],
			activitate: undefined,
			adaugare: false
		}

		this.setActivitate = this.setActivitate.bind(this)
		this.inapoi = this.inapoi.bind(this)
		this.inscriereFacultate = this.inscriereFacultate.bind(this)
		this.refreshActivitati = this.refreshActivitati.bind(this)
		this.setAdaugare = this.setAdaugare.bind(this)
		this.setAdaugareFalse = this.setAdaugareFalse.bind(this)
	}

	inscriereFacultate = async cod => {
		await axios.post("https://19812b1dec7c456aba654301513f235c.vfs.cloud9.us-east-2.amazonaws.com/api/inscriere", {
			idUtilizator: this.props.utilizator.id,
			cod: cod
		})
		this.refreshActivitati()
	}

	setAdaugare = () => {
		this.setState({ adaugare: true })
	}

	setAdaugareFalse = () => {
		this.setState({ adaugare: false })
	}

	setActivitate = activitate => {
		this.setState({ activitate: activitate })
	}

	inapoi = () => {
		this.setState({ activitate: undefined })
	}

	refreshActivitati = () => {
		axios
			.get(`http://localhost:8080/api/activitate/${this.props.utilizator.id}`)
			.then(activitati => {
				// axios.get(`https://13.59.158.190:8080/api/activitati/${this.props.utilizator.id}`).then(activitati => {
				this.setState({
					trecute: activitati.data.trecute,
					actuale: activitati.data.actuale,
					altele: activitati.data.altele
				})
			})
	}

	componentDidMount = () => {
		axios.get("http://localhost:8080/api/tipFeedback").then(tipFeedbacks => {
			// axios.get("https://13.59.158.190:8080/api/tipFeedback").then(tipFeedbacks => {
			this.setState({
				tipFeedbacks: tipFeedbacks.data
			})
		})
		this.refreshActivitati()
	}
	render() {
		return (
			<div>
				{this.state.activitate ? (
					this.props.utilizator.esteProfesor ? (
						<ActivitateProfesor
							{...this.state.activitate}
							tipFeedbacks={this.state.tipFeedbacks}
							inapoi={this.inapoi}
						></ActivitateProfesor>
					) : (
						<Activitate
							{...this.state.activitate}
							tipFeedbacks={this.state.tipFeedbacks}
							inapoi={this.inapoi}
						></Activitate>
					)
				) : (
					<DashboardMode
						inscriereFacultate={this.inscriereFacultate}
						trecute={this.state.trecute}
						utilizator={this.props.utilizator}
						actuale={this.state.actuale}
						altele={this.state.altele}
						tipFeedbacks={this.state.tipFeedbacks}
						setActivitate={this.setActivitate}
					></DashboardMode>
				)}
				{this.props.utilizator.esteProfesor && !this.state.adaugare && !this.state.activitate ? (
					<button className='btn btn-sm btn-success ml-5 mt-3' onClick={this.setAdaugare}>
						Adauga activitate
					</button>
				) : null}
				{this.state.adaugare && !this.state.activitate ? (
					<AdaugareActivitate
						utilizator={this.props.utilizator}
						setAdaugareFalse={this.setAdaugareFalse}
						refreshActivitati={this.refreshActivitati}
					></AdaugareActivitate>
				) : null}
			</div>
		)
	}
}

export default Home

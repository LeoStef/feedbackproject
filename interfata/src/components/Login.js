import React, { Component } from "react"

class Login extends Component {
	constructor(props) {
		super(props)

		this.state = {
			email: "",
			parola: ""
		}

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	handleSubmit() {
		this.props.autentificare(this.state.email, this.state.parola)
	}

	render() {
		return (
			<div className='col-sm-3'>
				<h3>Autentificare</h3>
				<form>
					<input
						value={this.state.email}
						placeholder='email'
						className='form-control'
						type='text'
						name='email'
						onChange={this.handleChange}
						required
					/>
					<input
						value={this.state.parola}
						placeholder='parola'
						className='form-control'
						type='password'
						name='parola'
						onChange={this.handleChange}
						required
					/>
					<button
						type='button'
						className='btn btn-sm btn-primary pull-right mt-2'
						onClick={this.handleSubmit}
					>
						Login
					</button>
				</form>
			</div>
		)
	}
}

export default Login

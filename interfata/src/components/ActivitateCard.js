import React from "react"
import Modal from "react-modal"

const customStyles = {
	content: {
		top: "50%",
		left: "50%",
		right: "auto",
		bottom: "auto",
		marginRight: "-50%",
		transform: "translate(-50%, -50%)"
	}
}

class ActivitateCard extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			cod: "",
			arataModal: false
		}

		this.changeCode = this.changeCode.bind(this)
		this.inscriere = this.inscriere.bind(this)
		this.setArataModalTrue = this.setArataModalTrue.bind(this)
		this.setArataModalFalse = this.setArataModalFalse.bind(this)
	}

	inscriere() {
		if (this.state.cod == this.props.id) {
			this.setState({ arataModal: false, cod: "" })
			this.props.inscriereFacultate(this.state.cod)
		}
	}

	changeCode(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	setArataModalTrue() {
		this.setState({ arataModal: true })
	}

	setArataModalFalse() {
		this.setState({ arataModal: false, cod: "" })
	}

	render() {
		return (
			<>
				<div className='card'>
					<div className='card-body  ml-1'>
						<div className='card-title'>{this.props.descriere}</div>
						<div className='card-text'>
							<div>Data: {new Date(this.props.data).toLocaleDateString()}</div>
							<div className='mb-3'>Timp: {this.props.timp}</div>
							<button
								hidden={!this.props.arataAccesare}
								className='btn btn-sm btn-primary'
								onClick={() =>
									this.props.setActivitate({
										id: this.props.id,
										descriere: this.props.descriere,
										data: this.props.data,
										timp: this.props.timp
									})
								}
							>
								Acceseaza
							</button>
							<button
								hidden={!this.props.arataInscriere}
								className='btn btn-sm btn-success float-right'
								onClick={this.setArataModalTrue}
							>
								Inscrie-te
							</button>
							<Modal
								isOpen={this.state.arataModal}
								onRequestClose={this.setArataModalFalse}
								style={customStyles}
								contentLabel='Modal'
							>
								<div>Inscriere</div>
								<form>
									<label htmlFor='cod'></label>
									<input value={this.state.cod} onChange={this.changeCode} name='cod' />
									<br></br>
									<br></br>
									<button className='btn btn-sm btn-primary' onClick={this.inscriere}>
										Inscrie-te
									</button>
									<button
										className='btn btn-sm btn-warning float-right'
										onClick={this.setArataModalFalse}
									>
										Inchide
									</button>
								</form>
							</Modal>
						</div>
					</div>
				</div>
			</>
		)
	}
}

export default ActivitateCard

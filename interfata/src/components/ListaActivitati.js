import React from "react"

import ActivitateCard from "./ActivitateCard"

class ListaActivitati extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<div className='mt-4'>
				<h3>{this.props.titlu}</h3>
				<div className='row'>
					{this.props.activitati.map(activitate => (
						<div className='col-sm-2' key={activitate.id}>
							<ActivitateCard
								arataAccesare={this.props.arataAccesare}
								arataInscriere={this.props.arataInscriere}
								{...activitate}
								inscriereFacultate={this.props.inscriereFacultate}
								setActivitate={this.props.setActivitate}
							></ActivitateCard>
						</div>
					))}
				</div>
			</div>
		)
	}
}

export default ListaActivitati

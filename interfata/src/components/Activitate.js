import React from "react"

import ListaReaction from "./ListaReaction"

class Activitate extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<>
				<div className='ml-3'>
					<h2>{this.props.descriere}</h2>
					<div>Data: {new Date(this.props.data).toLocaleDateString()}</div>
					<button className='btn btn-sm btn-warning mt-2' onClick={this.props.inapoi}>
						Inapoi
					</button>
				</div>
				<br />
				<br />
				<ListaReaction
					idActivitate={this.props.id}
					tipFeedbacks={this.props.tipFeedbacks}
				></ListaReaction>
			</>
		)
	}
}

export default Activitate

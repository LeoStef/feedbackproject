import React from "react"

import Reaction from "./Reaction"

class ListaReaction extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<div className='ml-3'>
				<h2>Reactii</h2>
				<div className='row mr-3'>
					{this.props.tipFeedbacks.map(reactie => (
						<div className='col-sm-6' key={reactie.id}>
							<Reaction idActivitate={this.props.idActivitate} {...reactie}></Reaction>
						</div>
					))}
				</div>
			</div>
		)
	}
}

export default ListaReaction

import React from "react"

class ListaReactionProfesor extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<div className='ml-3'>
				<h2>Reactii</h2>
				<div className='row mr-3'>
					{this.props.tipFeedbacks.map(reactie => (
						<div className='col-sm-6' key={reactie.id}>
							<div className='mt-5'>
								<button
									className='btn btn-danger mr-2'
									style={{ width: 100 + "%", height: 200 + "px" }}
								>
									{reactie.nume}
									<br />
									<h3>
										{this.props.feedbacks.filter(feedback => feedback.idTip === reactie.id).length}
									</h3>
								</button>
							</div>
						</div>
					))}
				</div>
			</div>
		)
	}
}

export default ListaReactionProfesor

import React from "react"

import ListaActivitati from "./ListaActivitati"

class DashboardMode extends React.Component {
	state = {}
	render() {
		return (
			<div className='ml-5'>
				<h2>Activitati</h2>
				{this.props.actuale && this.props.actuale.length > 0 ? (
					<ListaActivitati
						titlu='Actuale'
						inscriereFacultate={this.props.inscriereFacultate}
						activitati={this.props.actuale}
						setActivitate={this.props.setActivitate}
						inapoi={this.props.inapoi}
						arataAccesare={true}
					></ListaActivitati>
				) : (
					<h2>Nicio activitate actuala</h2>
				)}
				<br></br>
				{this.props.trecute && this.props.trecute.length > 0 ? (
					<ListaActivitati
						titlu='Trecute'
						inscriereFacultate={this.props.inscriereFacultate}
						activitati={this.props.trecute}
						setActivitate={this.props.setActivitate}
						inapoi={this.props.inapoi}
						arataAccesare={this.props.utilizator.esteProfesor}
					></ListaActivitati>
				) : (
					<h2>Nicio activitate trecuta</h2>
				)}
				{!this.props.utilizator.esteProfesor ||
				(this.props.altele && this.props.altele.length > 0) ? (
					<ListaActivitati
						titlu='Altele'
						inscriereFacultate={this.props.inscriereFacultate}
						activitati={this.props.altele || []}
						setActivitate={this.props.setActivitate}
						inapoi={this.props.inapoi}
						arataInscriere={true}
						arataAccesare={false}
					></ListaActivitati>
				) : null}
			</div>
		)
	}
}

export default DashboardMode

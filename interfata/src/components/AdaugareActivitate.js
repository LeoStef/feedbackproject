import React from "react"
import axios from "axios"

import DateTimePicker from "react-datetime-picker"

class AdaugareActivitate extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			descriere: "",
			data: new Date(),
			timp: ""
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}
	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async handleSubmit() {
		const activitate = {
			descriere: this.state.descriere,
			data: this.state.data,
			timp: this.state.timp,
			idProfesor: this.props.utilizator.id
		}
		await axios.post("https://19812b1dec7c456aba654301513f235c.vfs.cloud9.us-east-2.amazonaws.com/api/activitate", activitate)
		this.props.setAdaugareFalse()
		this.props.refreshActivitati()
	}
	render() {
		return (
			<div className='ml-4 col-sm-3'>
				<form>
					<label>Descriere</label>
					<input
						value={this.state.descriere}
						className='form-control'
						type='text'
						name='descriere'
						onChange={this.handleChange}
					/>
					<br />
					<label>Data</label>
					<DateTimePicker
						name='data'
						onChange={date => this.setState({ data: new Date(date) })}
						value={this.state.data}
					/>
					{/* <input
						value={this.state.data}
						className='form-control'
						type='date'
						name='data'
						onChange={this.handleChange}
					/> */}
					<br />
					<label>Timp</label>
					<input
						value={this.state.timp}
						className='form-control'
						type='text'
						name='timp'
						onChange={this.handleChange}
					/>
					<br />
					<button className='btn btn-sm btn-warning' onClick={this.props.setAdaugareFalse}>
						Inapoi
					</button>
					<button
						type='button'
						className='btn btn-sm btn-primary pull-right'
						onClick={this.handleSubmit}
					>
						Adauga
					</button>
				</form>
			</div>
		)
	}
}

export default AdaugareActivitate

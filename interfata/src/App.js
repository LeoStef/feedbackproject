import React from "react"
import "./App.css"
import axios from "axios"

import Login from "./components/Login"
import Home from "./components/Home"

class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			esteAutentificat: false,
			utilizator: undefined
		}

		this.autentificare = this.autentificare.bind(this)
	}

	autentificare(email, parola) {
		axios.get("https://19812b1dec7c456aba654301513f235c.vfs.cloud9.us-east-2.amazonaws.com:8080/api/utilizator/" + email).then(utilizator => {
			if (utilizator.data && utilizator.data.parola === parola) {
				this.setState({
					esteAutentificat: true,
					utilizator: utilizator.data
				})
			} else {
				alert("Wrong password")
			}
		})
	}

	render() {
		return (
			<div className='mt-3'>
				{this.state.esteAutentificat ? (
					<Home utilizator={this.state.utilizator}></Home>
				) : (
					<Login autentificare={this.autentificare}></Login>
				)}
			</div>
		)
	}
}

export default App

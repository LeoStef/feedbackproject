const tipFeedbackService = require("./../service/tipFeedback")

const getTipFeedback = async (req, res, next) => {
	try {
		const tipFeedbacks = await tipFeedbackService.get()
		res.status(200).send(tipFeedbacks)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

module.exports = {
	getTipFeedback
}

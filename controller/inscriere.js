// var router = express.Router({ mergeParams: true })

const inscriereService = require("../service/inscriere")

const createInscriere = async (req, res, next) => {
	const inscriere = req.body
	if (inscriere.cod && inscriere.idUtilizator) {
		await inscriereService.create(inscriere)
		res.status(201).send({
			message: "Inscriere creata cu succes."
		})
	} else {
		res.status(400).send({
			message: "Date incorecte."
		})
	}
}

module.exports = {
	createInscriere
}

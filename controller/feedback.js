const feedbackService = require("./../service/feedback")

const getFeedback = async (req, res, next) => {
	try {
		const feedbacks = await feedbackService.get(req.params.idActivitate)
		res.status(200).send(feedbacks)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createFeedback = async (req, res, next) => {
	const feedback = req.body
	if (feedback.idActivitate && feedback.idTip) {
		await feedbackService.create(feedback)
		res.status(201).send({
			message: "Feedback creat cu succes."
		})
	} else {
		res.status(400).send({
			message: "Date incorecte."
		})
	}
}

module.exports = {
	getFeedback,
	createFeedback
}

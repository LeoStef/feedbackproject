const utilizatorService = require("./../service/utilizator")

const getUtilizator = async (req, res, next) => {
	try {
		const utilizator = await utilizatorService.getByEmail(req.params.email)
		res.status(200).send(utilizator)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createUtilizator = async (req, res, next) => {
	const utilizator = req.body
	if (utilizator.nume && utilizator.prenume && utilizator.email && utilizator.esteProfesor) {
		await utilizatorService.create(utilizator)
		res.status(201).send({
			message: "Utilizator creata cu succes."
		})
	} else {
		res.status(400).send({
			message: "Date incorecte."
		})
	}
}

module.exports = {
	getUtilizator,
	createUtilizator
}

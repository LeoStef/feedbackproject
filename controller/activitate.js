const activitateService = require("./../service/activitate")

const getActivitate = async (req, res, next) => {
	try {
		const activitati = await activitateService.getById(req.params.id)
		res.status(200).send(activitati)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createActivitate = async (req, res, next) => {
	const activitate = req.body
	if (activitate.data && activitate.descriere && activitate.timp && activitate.idProfesor) {
		await activitateService.create(activitate)
		res.status(201).send({
			message: "Activitate creata cu succes."
		})
	} else {
		res.status(400).send({
			message: "Date incorecte."
		})
	}
}

module.exports = {
	getActivitate,
	createActivitate
}
